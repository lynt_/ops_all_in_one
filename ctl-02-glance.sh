#!/bin/bash


source functions.sh
source info.sh

########################
### GLANCE #############
########################

# [1] 	Add users and others for Glance in Keystone.

source ~/keystonerc

function glance_register () {
    echocolor "Add users and others for Glance in Keystone."
    sleep 3
    openstack user create --domain default --project service --password $GLANCE_USER_PASS $GLANCE_USER 
    openstack role add --project service --user $GLANCE_USER admin 
    openstack service create --name glance --description "OpenStack Image service" image
    openstack endpoint create --region RegionOne image public http://$HOST_CTL:9292 
    openstack endpoint create --region RegionOne image internal http://$HOST_CTL:9292 
    openstack endpoint create --region RegionOne image admin http://$HOST_CTL:9292 

    echocolor "Added users and others for Glance in Keystone."
    sleep 3
}

# [2] 	Add a User and Database on MariaDB for Glance. 
function add_user () {

    echocolor "Create database for glance service"
    sleep 3
        cat << EOF | mysql
CREATE DATABASE glance;
GRANT ALL PRIVILEGES ON glance.* TO glance@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON glance.* TO glance@'%' IDENTIFIED BY 'password';
FLUSH PRIVILEGES;
EOF
	echocolor "Done create database for glance service"
    sleep 3
}


# [3] Install Glance 

function install_glance () {
    echocolor "Install Glance"
    sleep 3
    apt -y install glance 
    echocolor "Done Install Glance"
    sleep 3

}
# [4] 	Configure Glance.
function config_glance () {

    echocolor "Configure glance"
    sleep 3
    glance_config=/etc/glance/glance-api.conf
    glance_config_bak="/etc/glance/glance-api.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"

    mv $glance_config $glance_config_bak
    touch $glance_config

    ops_add $glance_config DEFAULT bind_host $HOST_CTL_IP
    ops_add $glance_config DEFAULT transport_url rabbit://$RABBIT_USER:$RABBIT_PASS@$HOST_CTL
    
    ops_add $glance_config glance_store stores file,http
    ops_add $glance_config glance_store default_store file
    ops_add $glance_config glance_store filesystem_store_datadir  $GLANCE_IMG_LOCATION

    ops_add $glance_config database connection mysql+pymysql://$MYSQL_GLANCE_USER:$MYSQL_GLANCE_USER_PASS@$HOST_CTL/glance

    ops_add $glance_config keystone_authtoken www_authenticate_uri http://$HOST_CTL:5000
    ops_add $glance_config keystone_authtoken auth_url http://$HOST_CTL:5000
    ops_add $glance_config keystone_authtoken memcached_servers $HOST_CTL:11211
    ops_add $glance_config keystone_authtoken auth_type password
    ops_add $glance_config keystone_authtoken project_domain_name default
    ops_add $glance_config keystone_authtoken user_domain_name default
    ops_add $glance_config keystone_authtoken project_name service
    ops_add $glance_config keystone_authtoken username $GLANCE_USER
    ops_add $glance_config keystone_authtoken password $GLANCE_USER_PASS

    ops_add $glance_config paste_deploy flavor keystone

    chmod 640 $glance_config
    chown root:glance $glance_config
    su -s /bin/bash glance -c "glance-manage db_sync" 
    systemctl restart glance-api 
    systemctl enable glance-api 
    systemctl restart apache2

    echocolor "Done config glance"
    sleep 3
    echocolor "DONE"
    sleep 3


}

#####################################
######  RUN   #######################
#####################################

# [1] 	Add users and others for Glance in Keystone.
glance_register
# [2] 	Add a User and Database on MariaDB for Glance. 
add_user
# [3] Install Glance
install_glance
# [4] 	Configure Glance.
config_glance




