#!/bin/bash

source functions.sh
source info.sh

# [1] Download Ubuntu_22.04 image

function download () {
    echocolor "Download Ubuntu server 22.04 image"
    sleep 3

    cd $GLANCE_IMG_LOCATION
    curl http://releases.ubuntu.com/jammy/ubuntu-22.04.2-live-server-amd64.iso --output ubuntu_22.04.iso

    echocolor "Done download Ubuntu server 22.04 image"
    sleep 3

    echocolor "Download cirris 0.4.0 image"
    sleep 3
    apt install wget -y
    wget http://download.cirros-cloud.net/0.4.0/cirros-0.4.0-x86_64-disk.img

    echocolor "Done download cirris 0.4.0 image"
    sleep 3
}


# [2] add image to Glance

function add_image () {
    echocolor "add image to Glance"
    sleep 3
    openstack image create "Ubuntu2204" --file $GLANCE_IMG_LOCATION/ubuntu_22.04.iso --disk-format qcow2 --container-format bare --public

    openstack image create "Cirros0.4.0" --file $GLANCE_IMG_LOCATION/cirros-0.4.0-x86_64-disk.img --disk-format qcow2 --container-format bare --public 
    echocolor "Done add image to Glance"
    sleep 3 

    echocolor "confirm settings"
    sleep 3
    openstack image list 
    sleep 3
    echocolor "DONE"
}

#########################################
########   RUN   #######################
#########################################

# [1] Download Ubuntu_22.04 image
download
# [2] add image to Glance
add_image
